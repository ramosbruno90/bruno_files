import io
io.__file__
import sys, os
from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--start-maximized")
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", chrome_options=options)
driver.get("http://apps.trt6.jus.br/certidao/emitir")

cnpjInput = driver.find_element_by_xpath("//*[@id='id-field-documento']")
cnpjInput.send_keys("21101794000150")
driver.close()
