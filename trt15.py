import io
io.__file__
import sys, os
from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = Options()
options.add_argument("--start-maximized")
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", chrome_options=options)
driver.get("http://portal.trt15.jus.br/ceat;jsessionid=B9F2728A4A562103B6B1B85D4228377F.lr2")
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it("_48_INSTANCE_3cAzqZBHYNks_iframe"))

cnpjInput = driver.find_element_by_xpath("//*[@id='certidaoActionForm:j_id23:doctoPesquisa']")
cnpjInput.send_keys("14136629000178")
driver.close()
