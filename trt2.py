import io
io.__file__
import sys, os
from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--start-maximized")
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", chrome_options=options)
driver.get("http://aplicacoes10.trtsp.jus.br/certidao_trabalhista_eletronica/public/index.php/index/solicitacao")
optInput = driver.find_element_by_xpath("//*[@id='tipoDocumentoPesquisado-2']")
optInput.click()
cnpjInput = driver.find_element_by_xpath("//*[@id='numeroDocumentoPesquisado']")
cnpjInput.send_keys("14136629000178")
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

box = (354,261,534,343)
base = ("trt2_sessao_")
captcha_name = 'captcha' + '14136629000178' + '.png'
screen_name = base + '14136629000178' + '.png'
driver.get_screenshot_as_file(screen_name)
im = Image.open(screen_name)
croped = im.crop(box)
croped.save(captcha_name)
os.remove(screen_name)
driver.close()
