import io
io.__file__
import sys, os
from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--start-maximized")')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", chrome_options=options)
driver.get("http://www4.trt23.jus.br/e-certidao-web/emissao")

nameInput = driver.find_element_by_xpath("//*[@id='form:j_idt13']")
nameInput.send_keys("Bruno Ramos")
cpfInput = driver.find_element_by_xpath("//*[@id='form:j_idt17']")
cpfInput.send_keys("12345678909")
nameInput = driver.find_element_by_xpath("//*[@id='form:j_idt26']")
nameInput.send_keys("CEZAR GONCALVES ALEXANDRE DA SILVA 42425785809")
cnpjInput = driver.find_element_by_xpath("//*[@id='form:j_idt31']")
cnpjInput.send_keys("21101794000150")

driver.close()
