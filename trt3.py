import io
io.__file__
import sys, os
from PIL import Image
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = Options()
options.add_argument("--start-maximized")
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", chrome_options=options)
driver.get("http://as3.trt3.jus.br/certidao/feitosTrabalhistas/aba1.emissao.htm;jsessionid=PlXI6zrEz7aoy5QiK1wVWiqu%E2%80%8E%E2%80%8F")
wait = WebDriverWait(driver, 10)

myElem = WebDriverWait(driver, 1).until(EC.presence_of_element_located((By.XPATH, "//*[@id='form:tipoPessoa']/tbody/tr/td[3]/div")))
optInput = driver.find_element_by_xpath("//*[@id='form:tipoPessoa']/tbody/tr/td[3]/div")
optInput.click()
myElem = WebDriverWait(driver, 1).until(EC.presence_of_element_located((By.ID, 'form:inputCNPJ')))
cnpjInput = driver.find_element_by_xpath("//*[@id='form:inputCNPJ']")
cnpjInput.send_keys("014.136.629/0001-78")
optInput = driver.find_element_by_xpath("//*[@id='form:nomeReceitaCNPJ']")
optInput.click()

element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "form:nomeReceitaCNPJ")))
time.sleep(1)
box = (217,267,339,310)
base = ("trt3_sessao_")
captcha_name = 'captcha' + '14136629000178' + '.png'
screen_name = base + '14136629000178' + '.png'

driver.get_screenshot_as_file(screen_name)
im = Image.open(screen_name)
croped = im.crop(box)
croped.save(captcha_name)
os.remove(screen_name)
driver.close()
